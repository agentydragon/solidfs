import abc
import rdflib


class Fetcher(abc.ABC):
    @abc.abstractmethod
    def fetch(self, url: str) -> bytes:
        pass

    @abc.abstractmethod
    def put(self, url: str, content: bytes,
            mime_type: str) -> None:  #, mime_type: str) -> None:
        pass
