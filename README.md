# solidfs

FUSE filesystem mounting a Solid pod.

## Demo

Demo on YouTube: https://youtu.be/6p75rPzE3Zs

## Requirements

* Install [Bazel](http://bazel.build) and `libfuse3-dev` (e.g., `sudo apt
  install libfuse3-dev` on Ubuntu).

## Running

```bash
mkdir -p fusemount
bazel run //:solidfs_main -- \
  --mountpoint=$(pwd)/fusemount \
  --debug_fuse \
  --issuer=https://solidcommunity.net \
  --root_container=https://agentydragon.solidcommunity.net/
```

### Unmount

```bash
fusermount -u $(pwd)/fusemount
```
