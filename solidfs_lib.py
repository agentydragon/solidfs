import bidict
import rdflib
import io
import errno
import pyfuse3
import stat
import os
from absl import logging
import dataclasses
from typing import Optional, List
import datetime

LDP = rdflib.Namespace('http://www.w3.org/ns/ldp#')
RDF = rdflib.Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
ST = rdflib.Namespace('http://www.w3.org/ns/posix/stat#')

_RDF_TYPE = RDF['type']
_LDP_CONTAINS = LDP['contains']
_LDP_CONTAINER = LDP['Container']
_LDP_RESOURCE = LDP['Resource']
_ST_SIZE = ST['size']
_ST_MTIME = ST['mtime']


@dataclasses.dataclass
class NodeInfo:
    url: str
    inode: int
    open_file_handles: List['FileHandle']
    is_directory: bool
    write_buffer: Optional[bytes]
    size: int
    stamp: int

    def is_open_for_reading(self):
        return any(open_file_handles, lambda fh: fh.mode == os.O_RDONLY)

    def is_open_for_writing(self):
        return any(open_file_handles, lambda fh: fh.mode == os.O_WRONLY)

    def is_open(self):
        return len(open_file_handles) > 0

    def get_attrs(self) -> pyfuse3.EntryAttributes:
        entry = pyfuse3.EntryAttributes()
        if self.is_directory:
            entry.st_mode = (stat.S_IFDIR | 0o755)
        else:
            entry.st_mode = (stat.S_IFREG | 0o644)
        entry.st_size = self.size
        entry.st_atime_ns = self.stamp
        entry.st_ctime_ns = self.stamp
        entry.st_mtime_ns = self.stamp
        entry.st_gid = os.getgid()
        entry.st_uid = os.getuid()
        entry.st_ino = self.inode
        return entry


@dataclasses.dataclass
class FileHandle:
    # TODO: mode
    node: NodeInfo
    fh: int

    # either os.O_RDONLY or os.O_WRONLY
    mode: int


class SolidFS(pyfuse3.Operations):
    # TODO: rename, rmdir, fsync, fsyncdir
    def __init__(self, fetcher, root_container):
        super(SolidFS, self).__init__()

        self._fetcher = fetcher
        self._root_container = root_container

        # url -> rdflib.Graph
        #self._graph_cache = {}

        # For directories: stored URL has to end with '/'
        root_node_info = NodeInfo(url=self._root_container,
                                  inode=pyfuse3.ROOT_INODE,
                                  open_file_handles=[],
                                  is_directory=True,
                                  write_buffer=None,
                                  size=0,
                                  stamp=0)
        # Note: not in any particular order
        self._nodes = [root_node_info]

        self._next_inode_id = pyfuse3.ROOT_INODE + 1
        self._next_file_handle = 1000

        # self._new_content = {}

    def _get_graph(self, url):
        logging.info("_get_graph(%s)", url)
        #if url in self._graph_cache:
        #    return self._graph_cache[url]
        #else:
        graph = rdflib.Graph()
        data = self._fetcher.fetch(url)
        # HACK: rdflib does not like 'c:me ter:account n0:.'
        # TODO: file a bug against rdflib
        data = data.replace(b'n0:.\n', b'n0: .\n')
        #print(data)
        graph.parse(data=data, format='turtle', publicID=url)
        #self._graph_cache[url] = graph
        return graph

    def _find_node_info_by_inode(self, inode: int) -> Optional[NodeInfo]:
        for node in self._nodes:
            if node.inode == inode:
                return node
        return None

    def _find_node_info_by_url(self, url: str) -> Optional[NodeInfo]:
        for node in self._nodes:
            if node.url == url:
                return node
        return None

    def _find_file_handle_by_fh(self, fh: int) -> Optional[FileHandle]:
        for node in self._nodes:
            for file_handle in node.open_file_handles:
                if file_handle.fh == fh:
                    return file_handle
        return None

    async def getattr(self, inode, ctx=None):
        # https://agentydragon.solidcommunity.net/profile/
        node_info = self._find_node_info_by_inode(inode)

        if not node_info:
            logging.info("getattr on unknown inode %d", inode)
            raise pyfuse3.FUSEError(errno.ENOENT)

        return node_info.get_attrs()

    async def releasedir(self, fh):
        file_handle = self._find_file_handle_by_fh(fh)
        if not file_handle:
            # TODO
            logging.info("%s not a handle", file_handle)
            raise pyfuse3.FUSEError(errno.ENOENT)
        if not file_handle.node.is_directory:
            logging.info("%s not a directory handle", file_handle)
            raise pyfuse3.FUSEError(errno.ENOTDIR)

        file_handle.node.open_file_handles.remove(file_handle)

    async def lookup(self, parent_inode, name, ctx=None):
        parent_node_info = self._find_node_info_by_inode(parent_inode)
        parent_url = parent_node_info.url

        self._make_inodes_for_directory(parent_url,
                                        graph=self._get_graph(parent_url))

        child_url = (parent_url + name.decode('utf-8'))
        if (node_info := self._find_node_info_by_url(child_url)):
            logging.info("child URL %s does exist: %s", child_url, node_info)
            return node_info.get_attrs()

        child_url = (parent_url + name.decode('utf-8') + '/')
        if (node_info := self._find_node_info_by_url(child_url)):
            logging.info("child URL %s does exist: %s", child_url, node_info)
            return node_info.get_attrs()

        logging.info("child url for %s in %s does not exist...", name,
                     parent_url)
        raise pyfuse3.FUSEError(errno.ENOENT)

    async def opendir(self, inode, ctx=None):
        # TODO(prvak): check that it is actually a directory
        node_info = self._find_node_info_by_inode(inode)
        if not node_info:
            logging.info("opendir on unknown inode %d", inode)
            raise pyfuse3.FUSEError(errno.ENOENT)
        if not node_info.is_directory:
            logging.info("opendir not on directory %d", inode)
            raise pyfuse3.FUSEError(errno.ENOTDIR)

        fh = self._next_file_handle
        self._next_file_handle += 1
        handle = FileHandle(node=node_info, fh=fh, mode=os.O_RDONLY)
        node_info.open_file_handles.append(handle)

        return fh

    async def mkdir(self, parent_inode, name, mode, ctx=None):
        # return an EntryAttributes instance
        logging.info("mkdir(parent_inode=%d, name=%s, mode=%d)", parent_inode,
                     name, mode)
        parent_node_info = self._find_node_info_by_inode(parent_inode)
        child_url = parent_node_info.url + name.decode('utf-8') + '/'

        if not parent_node_info.is_directory:
            raise pyfuse3.FUSEError(errno.ENODIR)

        if (node_info := self._find_node_info_by_url(child_url)):
            logging.info("child URL %s already exists: %s", child_url,
                         node_info)
            raise pyfuse3.FUSEError(errno.EEXIST)

        graph = rdflib.Graph()
        graph.add((rdflib.URIRef(child_url), _RDF_TYPE, _LDP_CONTAINER))

        self._fetcher.put(url=child_url,
                          content=graph.serialize(format='turtle'),
                          mime_type='text/turtle')

        inode = self._next_inode_id
        node_info = NodeInfo(url=child_url,
                             inode=inode,
                             open_file_handles=[],
                             is_directory=True,
                             write_buffer=None,
                             size=0,
                             stamp=0)
        self._next_inode_id += 1

        return node_info.get_attrs()

    def _make_inodes_for_directory(self, url, graph):
        container = rdflib.URIRef(url)

        assert (container, _RDF_TYPE, _LDP_CONTAINER) in graph

        # From the graph, load its containments.
        objs = graph.objects(container, _LDP_CONTAINS)

        urls = []

        prefix = url
        if not prefix.endswith('/'):
            prefix = prefix + '/'

        for container_item in objs:
            logging.info("%s", container_item)
            # should start with the container
            assert isinstance(container_item, rdflib.term.URIRef)
            url = container_item.toPython()
            assert url.startswith(prefix)

            # Allocate inodes for everything in the graph.
            assert not url.endswith('//')
            node_info = self._find_node_info_by_url(url)
            if node_info:
                logging.info("inode for %s is already %s", url, node_info)
                continue

            is_directory = ((rdflib.URIRef(url), _RDF_TYPE, _LDP_CONTAINER)
                            in graph)
            inode = self._next_inode_id

            if is_directory:
                size = 0
            else:
                value = graph.value(subject=rdflib.URIRef(url),
                                    predicate=_ST_SIZE,
                                    any=False)
                assert value is not None
                size = value.toPython()

            stamp_value = graph.value(subject=rdflib.URIRef(url),
                                      predicate=_ST_MTIME,
                                      any=False)
            if stamp_value:
                stamp = int(stamp_value.value)
            else:
                stamp = 0

            node_info = NodeInfo(
                url=url,
                inode=inode,
                open_file_handles=[],
                # TODO(prvak): is_directory
                is_directory=is_directory,
                write_buffer=None,
                size=size,
                stamp=stamp)

            self._nodes.append(node_info)
            logging.info("allocating inode %s for %s", node_info, url)
            self._next_inode_id += 1

    async def readdir(self, fh, start_id, token):
        file_handle = self._find_file_handle_by_fh(fh)
        if not file_handle:
            logging.info("readdir not on file handle %d", fh)
            raise pyfuse3.FUSEError(errno.EINVAL)
        node_info = file_handle.node
        if not node_info.is_directory:
            logging.info("readdir on not directory %s", node_info)
            raise pyfuse3.FUSEError(errno.NOTDIR)
        url = node_info.url
        graph = self._get_graph(url)
        self._make_inodes_for_directory(url, graph)
        container = rdflib.URIRef(url)

        logging.info("readdir for %d (%s)", fh, url)

        # From the graph, load its containments.
        objs = graph.objects(container, _LDP_CONTAINS)

        urls = []

        prefix = url
        if not prefix.endswith('/'):
            prefix = prefix + '/'

        for container_item in objs:
            logging.info("%s", container_item)
            # should start with the container
            assert isinstance(container_item, rdflib.term.URIRef)
            theurl = container_item.toPython()
            assert theurl.startswith(prefix)

            urls.append(theurl)

        urls.sort()

        for i, url in enumerate(urls):
            if i < start_id:
                continue

            # should start with the container

            node = self._find_node_info_by_url(url)
            assert node

            attrs = node.get_attrs()

            filename = url[len(prefix):]
            if filename.endswith('/'):
                filename = filename[:-1]
            fn = filename.encode('utf-8')
            logging.info("reporting: %s %s", fn, attrs)
            do_continue = pyfuse3.readdir_reply(token,
                                                name=fn,
                                                attr=attrs,
                                                next_id=(i + 1))
            assert do_continue

    async def create(self, parent_inode, name, mode, flags, ctx):
        # TODO(agentydragon): file 'name' must not yet exist in 'parent_inode'
        logging.info("create(parent_inode=%d, name=%s, mode=%d)", parent_inode,
                     name, mode)
        parent_node_info = self._find_node_info_by_inode(parent_inode)
        child_url = parent_node_info.url + name.decode('utf-8')

        if (node_info := self._find_node_info_by_url(child_url)):
            logging.info("child URL %s already exists: %s", child_url,
                         node_info)
            raise pyfuse3.FUSEError(errno.EEXIST)

        if mode & os.O_RDONLY:
            logging.info("create mode %d has O_RDONLY", mode)
            raise pyfuse3.FUSEError(errno.ENOSYS)
        elif mode & os.O_RDWR:
            logging.info("create mode %d has O_RDWR", mode)
            raise pyfuse3.FUSEError(errno.ENOSYS)
        elif mode & os.O_WRONLY:
            logging.info("create mode %d has O_WRONLY, OK", mode)
        else:
            logging.info(
                "mode is: %d; has neither O_RDONLY, O_RDWR, O_WRONLY??", mode)

        # TODO(agentydragon): mark directory as having a new entry
        self._fetcher.put(url=child_url,
                          content=b'',
                          mime_type='application/octet-stream')
        #, mime_type='application/octet-stream',
        #    slug=name.decode('utf-8'))

        inode = self._next_inode_id
        node_info = NodeInfo(url=child_url,
                             inode=inode,
                             open_file_handles=[],
                             is_directory=False,
                             write_buffer=b'',
                             size=0,
                             stamp=int(datetime.datetime.now().timestamp()))
        file_handle = FileHandle(node=node_info,
                                 fh=self._next_file_handle,
                                 mode=os.O_WRONLY)
        node_info.open_file_handles.append(file_handle)
        self._nodes.append(node_info)
        self._next_inode_id += 1
        self._next_file_handle += 1

        attr = node_info.get_attrs()

        return pyfuse3.FileInfo(fh=file_handle.fh), attr

    async def open(self, inode, flags, ctx):
        node_info = self._find_node_info_by_inode(inode)
        if not node_info:
            logging.error("inode %d does not exist, cannot open it", inode)
            raise pyfuse3.FUSEError(errno.ENOENT)
        if flags & os.O_RDONLY:
            assert not node_info.is_open_for_writing()

            file_handle = FileHandle(node=node_info,
                                     fh=self._next_file_handle,
                                     mode=os.O_RDONLY)
            self._next_file_handle += 1
            node_info.open_file_handles.append(file_handle)
            return pyfuse3.FileInfo(fh=file_handle.fh)

        if flags & os.O_WRONLY:
            assert not node_info.is_open()
            assert node_info.write_buffer is None

            node_info.write_buffer = b''

            file_handle = FileHandle(node=node_info,
                                     fh=self._next_file_handle,
                                     mode=os.O_WRONLY)
            self._next_file_handle += 1
            node_info.open_file_handles.append(file_handle)
            return pyfuse3.FileInfo(fh=file_handle.fh)

        if flags & os.O_RDWR:
            # TODO
            raise pyfuse3.FUSEError(errno.ENOSYS)

        logging.info("ARGH")
        raise pyfuse3.FUSEError(errno.ENOSYS)

    async def release(self, fh):
        file_handle = self._find_file_handle_by_fh(fh)
        assert file_handle

        node_info = file_handle.node

        if file_handle.mode & os.O_WRONLY:
            assert node_info.write_buffer is not None
            logging.info("Flushing...")

            self._fetcher.put(
                #url=parent_node_info.url,
                url=node_info.url,
                content=node_info.write_buffer,
                mime_type='application/octet-stream')
            # TODO: ugh...
            #slug=node_info.rpartition('/')[2])
            #slug=node_info.filename.decode('utf-8'))

            # TODO: invalidate or something...?
            node_info.write_buffer = None

        file_handle.node.open_file_handles.remove(file_handle)

    async def read(self, fh, off, size):
        file_handle = self._find_file_handle_by_fh(fh)
        assert file_handle, f"no file handle found for {fh}"
        assert file_handle.mode == os.O_RDONLY

        url = file_handle.node.url
        data = self._fetcher.fetch(url)
        return data[off:off + size]

    async def write(self, fh, off, buf):
        """Write buf into fh at off."""
        file_handle = self._find_file_handle_by_fh(fh)
        assert file_handle
        assert file_handle.mode == os.O_WRONLY
        node_info = file_handle.node

        b = io.BytesIO(node_info.write_buffer)
        b.seek(off)
        written = b.write(buf)
        node_info.write_buffer = b.getvalue()
        return written

    #async def flush(self, fh):
    #    """Handle close() syscall. May be called multiple times for same fh
    #    if it was duplicated."""
    #    #file_handle = self._find_file_handle_by_fh(fh)
    #    #assert file_handle

    async def unlink(self, parent_inode, name, ctx=None):
        parent_node_info = self._find_node_info_by_inode(parent_inode)
        parent_url = parent_node_info.url

        self._make_inodes_for_directory(parent_url,
                                        graph=self._get_graph(parent_url))

        child_url = (parent_url + name.decode('utf-8'))
        if (node_info := self._find_node_info_by_url(child_url)):
            logging.info("child URL %s does exist: %s", child_url, node_info)

            self._fetcher.delete(url=child_url)
            return
            # TODO: actual removal should be deferred until later?

        child_url = (parent_url + name.decode('utf-8') + '/')
        if (node_info := self._find_node_info_by_url(child_url)):
            logging.info("child URL %s does exist: %s", child_url, node_info)

            self._fetcher.delete(url=child_url)
            return

        raise pyfuse3.FUSEError(errno.ENOSYS)
