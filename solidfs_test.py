from absl.testing import absltest
from solidfs import fetcher
from solidfs import solidfs_lib
from unittest import mock
import asyncio
import pyfuse3
from absl import logging
import stat
import os


class MockFetcher(fetcher.Fetcher):
    def __init__(self, content):
        self.content = content

    def fetch(self, url: str) -> bytes:
        assert url in self.content, f"url not in content: {url}"
        content = self.content[url]
        if isinstance(content, bytes):
            return content
        if isinstance(content, str):
            return content.encode('utf-8')
        raise Exception("wrong content?")

    def put(self, url: str, content: bytes, mime_type: str) -> None:
        raise Exception("can't put?")


_ROOT_TURTLE = '''@prefix : </#>.
@prefix n0: <>.
@prefix ldp: <http://www.w3.org/ns/ldp#>.
@prefix n1: </.well-known/>.
@prefix inbox: </inbox/>.
@prefix priv: </private/>.
@prefix pro: </profile/>.
@prefix pub: </public/>.
@prefix set: </settings/>.
@prefix st: <http://www.w3.org/ns/posix/stat#>.
@prefix vnd: <http://www.w3.org/ns/iana/media-types/image/vnd.microsoft.icon#>.
@prefix c: </profile/card#>.
@prefix ter: <http://www.w3.org/ns/solid/terms#>.
@prefix pl: <http://www.w3.org/ns/iana/media-types/text/plain#>.

n0:
    a ldp:BasicContainer, ldp:Container;
    ldp:contains n1:, </favicon.ico>, inbox:, priv:, pro:, pub:, </robots.txt>, set:;
    st:mtime 1612946944.068;
    st:size 4096.

n1:
    a ldp:BasicContainer, ldp:Container, ldp:Resource;
    st:mtime 1612454728.594;
    st:size 4096.

</favicon.ico>
    a vnd:Resource, ldp:Resource;
    st:mtime 1612454728.594;
    st:size 4286.

inbox:
    a ldp:BasicContainer, ldp:Container, ldp:Resource;
    st:mtime 1612454728.59;
    st:size 4096.

priv:
    a ldp:BasicContainer, ldp:Container, ldp:Resource;
    st:mtime 1612638421.474;
    st:size 4096.

pro:
    a ldp:BasicContainer, ldp:Container, ldp:Resource;
    st:mtime 1612454728.59;
    st:size 4096.

c:me ter:account n0:.

pub:
    a ldp:BasicContainer, ldp:Container, ldp:Resource;
    st:mtime 1612560169.99;
    st:size 4096.

</robots.txt>
    a pl:Resource, ldp:Resource;
    st:mtime 1612454728.586;
    st:size 83.

set:
    a ldp:BasicContainer, ldp:Container, ldp:Resource;
    st:mtime 1612454728.586;
    st:size 4096.'''

# https://agentydragon.solidcommunity.net/public/
_PRIVATE_DIR_TURTLE = '''@prefix : <#>.
@prefix priv: <>.
@prefix ldp: <http://www.w3.org/ns/ldp#>.
@prefix con: <concept/>.
@prefix wor: <worthy/>.
@prefix st: <http://www.w3.org/ns/posix/stat#>.

priv:
    a ldp:BasicContainer, ldp:Container;
    ldp:contains con:, wor:;
    st:mtime 1612638421.474;
    st:size 4096.

con:
    a ldp:BasicContainer, ldp:Container, ldp:Resource;
    st:mtime 1612559256.463;
    st:size 4096.

wor:
    a ldp:BasicContainer, ldp:Container, ldp:Resource;
    st:mtime 1612473636.582;
    st:size 4096.'''


class SolidFSTest(absltest.TestCase):
    @mock.patch('solidfs_lib.pyfuse3.readdir_reply')
    def test_readdir_on_root_container(self, mock_readdir_reply):
        fetcher = MockFetcher(
            content={'https://agentydragon.solidcommunity.net/': _ROOT_TURTLE})
        solidfs = solidfs_lib.SolidFS(
            fetcher=fetcher,
            root_container='https://agentydragon.solidcommunity.net/')
        mock_readdir_reply.return_value = True
        TOKEN = '123'
        dir_fh = asyncio.run(solidfs.opendir(inode=pyfuse3.ROOT_INODE))
        asyncio.run(solidfs.readdir(fh=dir_fh, start_id=0, token=TOKEN))
        asyncio.run(solidfs.releasedir(fh=dir_fh))
        calls = [
            mock.call(TOKEN, name=b'.well-known', attr=mock.ANY, next_id=1),
            mock.call(TOKEN, name=b'favicon.ico', attr=mock.ANY, next_id=2),
            mock.call(TOKEN, name=b'inbox', attr=mock.ANY, next_id=3),
            mock.call(TOKEN, name=b'private', attr=mock.ANY, next_id=4),
            mock.call(TOKEN, name=b'profile', attr=mock.ANY, next_id=5),
            mock.call(TOKEN, name=b'public', attr=mock.ANY, next_id=6),
            mock.call(TOKEN, name=b'robots.txt', attr=mock.ANY, next_id=7),
            mock.call(TOKEN, name=b'settings', attr=mock.ANY, next_id=8)
        ]
        mock_readdir_reply.assert_has_calls(calls)

        # Check types.
        expected_modes = [
            (stat.S_IFDIR | 0o755),  # .well-known
            (stat.S_IFREG | 0o644),  # favicon.ico
            (stat.S_IFDIR | 0o755),  # inbox
            (stat.S_IFDIR | 0o755),  # private
            (stat.S_IFDIR | 0o755),  # profile
            (stat.S_IFDIR | 0o755),  # public
            (stat.S_IFREG | 0o644),  # robots.txt
            (stat.S_IFDIR | 0o755),  # settings
        ]
        for call, expected_mode in zip(mock_readdir_reply.mock_calls,
                                       expected_modes):
            self.assertEquals(expected_mode, call.kwargs['attr'].st_mode)

    @mock.patch('solidfs_lib.pyfuse3.readdir_reply')
    def test_readdir_on_root_container_from_start_id(self, mock_readdir_reply):
        fetcher = MockFetcher(
            content={'https://agentydragon.solidcommunity.net/': _ROOT_TURTLE})
        solidfs = solidfs_lib.SolidFS(
            fetcher=fetcher,
            root_container='https://agentydragon.solidcommunity.net/')
        mock_readdir_reply.return_value = True
        TOKEN = '123'
        dir_fh = asyncio.run(solidfs.opendir(inode=pyfuse3.ROOT_INODE))
        asyncio.run(solidfs.readdir(fh=dir_fh, start_id=4, token=TOKEN))
        asyncio.run(solidfs.releasedir(fh=dir_fh))
        calls = [
            mock.call(TOKEN, name=b'profile', attr=mock.ANY, next_id=5),
            mock.call(TOKEN, name=b'public', attr=mock.ANY, next_id=6),
            mock.call(TOKEN, name=b'robots.txt', attr=mock.ANY, next_id=7),
            mock.call(TOKEN, name=b'settings', attr=mock.ANY, next_id=8)
        ]
        mock_readdir_reply.assert_has_calls(calls)

    # TODO: read a dir

    @mock.patch('solidfs_lib.pyfuse3.readdir_reply')
    def test_readdir_on_subdir(self, mock_readdir_reply):
        fetcher = MockFetcher(
            content={
                'https://agentydragon.solidcommunity.net/':
                _ROOT_TURTLE,
                'https://agentydragon.solidcommunity.net/private/':
                _PRIVATE_DIR_TURTLE
            })
        solidfs = solidfs_lib.SolidFS(
            fetcher=fetcher,
            root_container='https://agentydragon.solidcommunity.net/')
        mock_readdir_reply.return_value = True

        # Find inode of public dir.
        attrs = asyncio.run(
            solidfs.lookup(parent_inode=pyfuse3.ROOT_INODE, name=b'private'))
        READDIR_TOKEN = '123'
        dir_fh = asyncio.run(solidfs.opendir(inode=attrs.st_ino))
        asyncio.run(solidfs.readdir(fh=dir_fh, start_id=0,
                                    token=READDIR_TOKEN))
        asyncio.run(solidfs.releasedir(fh=dir_fh))

        calls = [
            mock.call(READDIR_TOKEN, name=b'concept', attr=mock.ANY,
                      next_id=1),
            mock.call(READDIR_TOKEN, name=b'worthy', attr=mock.ANY, next_id=2),
        ]
        mock_readdir_reply.assert_has_calls(calls)

    def test_create_empty_file_in_root(self):
        mock_fetcher = mock.create_autospec(spec=fetcher.Fetcher)
        mock_fetcher.fetch.side_effect = lambda url: {
            'https://agentydragon.solidcommunity.net/': _ROOT_TURTLE
        }[url]
        solidfs = solidfs_lib.SolidFS(
            fetcher=mock_fetcher,
            root_container='https://agentydragon.solidcommunity.net/')

        # Find inode of public dir.
        READDIR_TOKEN = '123'
        info, attrs = asyncio.run(
            solidfs.create(parent_inode=pyfuse3.ROOT_INODE,
                           name=b'test',
                           mode=os.O_WRONLY,
                           flags=None,
                           ctx=None))
        asyncio.run(solidfs.release(info.fh))

        mock_fetcher.put.assert_has_calls([
            mock.call('https://agentydragon.solidcommunity.net/test',
                      b'',
                      mime_type='application/octet-stream'),
            mock.call('https://agentydragon.solidcommunity.net/test',
                      b'',
                      mime_type='application/octet-stream'),
        ])

    def test_create_file_in_root_with_one_write(self):
        mock_fetcher = mock.create_autospec(spec=fetcher.Fetcher)
        mock_fetcher.fetch.side_effect = lambda url: {
            'https://agentydragon.solidcommunity.net/': _ROOT_TURTLE
        }[url]
        solidfs = solidfs_lib.SolidFS(
            fetcher=mock_fetcher,
            root_container='https://agentydragon.solidcommunity.net/')

        # Find inode of public dir.
        READDIR_TOKEN = '123'
        info, attrs = asyncio.run(
            solidfs.create(parent_inode=pyfuse3.ROOT_INODE,
                           name=b'test',
                           mode=os.O_WRONLY,
                           flags=None,
                           ctx=None))
        asyncio.run(solidfs.write(info.fh, off=0, buf=b'12345'))
        asyncio.run(solidfs.release(info.fh))

        mock_fetcher.put.assert_has_calls([
            mock.call('https://agentydragon.solidcommunity.net/test',
                      b'',
                      mime_type='application/octet-stream'),
            mock.call('https://agentydragon.solidcommunity.net/test',
                      b'12345',
                      mime_type='application/octet-stream'),
        ])

    def test_create_file_in_root_with_two_writes(self):
        mock_fetcher = mock.create_autospec(spec=fetcher.Fetcher)
        mock_fetcher.fetch.side_effect = lambda url: {
            'https://agentydragon.solidcommunity.net/': _ROOT_TURTLE
        }[url]
        solidfs = solidfs_lib.SolidFS(
            fetcher=mock_fetcher,
            root_container='https://agentydragon.solidcommunity.net/')

        # Find inode of public dir.
        READDIR_TOKEN = '123'
        info, attrs = asyncio.run(
            solidfs.create(parent_inode=pyfuse3.ROOT_INODE,
                           name=b'test',
                           mode=os.O_WRONLY,
                           flags=None,
                           ctx=None))
        asyncio.run(solidfs.write(info.fh, off=0, buf=b'12345'))
        asyncio.run(solidfs.write(info.fh, off=5, buf=b'67890'))
        asyncio.run(solidfs.release(info.fh))

        mock_fetcher.put.assert_has_calls([
            mock.call(url='https://agentydragon.solidcommunity.net/test',
                      content=b'',
                      mime_type='application/octet-stream'),
            mock.call(url='https://agentydragon.solidcommunity.net/test',
                      content=b'1234567890',
                      mime_type='application/octet-stream'),
        ])


if __name__ == '__main__':
    absltest.main()
