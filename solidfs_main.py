import multiprocessing

import abc
import bidict
import rdflib
from werkzeug import Request
from werkzeug import Response
from werkzeug import run_simple

import io
from absl import app, flags
import errno
import os
import pyfuse3
import trio
import stat

import base64
import datetime
import hashlib
import json
import re
import urllib

import jwcrypto
import jwcrypto.jwk
import jwcrypto.jws
import jwcrypto.jwt
import requests
from absl import app, flags, logging
from oic.oic import Client as OicClient
from oic.utils.authn.client import CLIENT_AUTHN_METHOD

from solidfs import solidfs_lib
from solidfs import oauth

_MOUNTPOINT = flags.DEFINE_string("mountpoint", None, "Mount point for the fs")
_DEBUG_FUSE = flags.DEFINE_boolean("debug_fuse", False,
                                   "Whether to debug FUSE")
_PORT = flags.DEFINE_integer('port', 3456, 'HTTP port to listen on')
_ISSUER = flags.DEFINE_string('issuer', 'https://solidcommunity.net/',
                              'Issuer')
_ROOT_CONTAINER = flags.DEFINE_string(
    'root_container', 'https://agentydragon.solidcommunity.net/',
    'Root container')


def main(_):
    fetcher = oauth.obtain_authenticated_fetcher(_ISSUER.value, _PORT.value)
    fs = solidfs_lib.SolidFS(
        fetcher=fetcher,
        root_container=_ROOT_CONTAINER.value,
    )
    fuse_options = set(pyfuse3.default_options)
    fuse_options.add('fsname=solidfs')
    if _DEBUG_FUSE.value:
        fuse_options.add('debug')
    pyfuse3.init(fs, _MOUNTPOINT.value, fuse_options)
    try:
        trio.run(pyfuse3.main)
    except:
        pyfuse3.close(unmount=False)
        raise

    pyfuse3.close()


if __name__ == '__main__':
    flags.mark_flags_as_required([
        _MOUNTPOINT.name,
        _ISSUER.name,
        _ROOT_CONTAINER.name,
    ])
    app.run(main)
