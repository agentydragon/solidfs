import multiprocessing
from werkzeug import Request
from werkzeug import Response
from werkzeug import run_simple
import os
import base64
import datetime
import hashlib
import json
import re
import urllib
import jwcrypto
import jwcrypto.jwk
import jwcrypto.jws
import jwcrypto.jwt
import requests
from absl import flags, logging
from oic.oic import Client as OicClient
from oic.utils.authn.client import CLIENT_AUTHN_METHOD

from solidfs import fetcher


def _make_random_string():
    x = base64.urlsafe_b64encode(os.urandom(40)).decode('utf-8')
    x = re.sub('[^a-zA-Z0-9]+', '', x)
    return x


def _make_verifier_challenge():
    code_verifier = _make_random_string()

    code_challenge = hashlib.sha256(code_verifier.encode('utf-8')).digest()
    code_challenge = base64.urlsafe_b64encode(code_challenge).decode('utf-8')
    code_challenge = code_challenge.replace('=', '')

    return code_verifier, code_challenge


def _register_client(provider_info, redirect_url: str):
    # Client registration.
    # https://pyoidc.readthedocs.io/en/latest/examples/rp.html#client-registration
    registration_response = OicClient(
        client_authn_method=CLIENT_AUTHN_METHOD).register(
            provider_info['registration_endpoint'],
            redirect_uris=[redirect_url])
    logging.info("Registration response: %s", registration_response)
    return registration_response['client_id']


def _make_token_for(keypair, uri, method):
    jwt = jwcrypto.jwt.JWT(header={
        "typ":
        "dpop+jwt",
        "alg":
        "ES256",
        "jwk":
        keypair.export(private_key=False, as_dict=True)
    },
                           claims={
                               "jti": _make_random_string(),
                               "htm": method,
                               "htu": uri,
                               "iat": int(datetime.datetime.now().timestamp())
                           })
    jwt.make_signed_token(keypair)
    return jwt.serialize()


def _get_redirect_url(port: int):
    return f"http://localhost:{port}"


def _get_token(q: multiprocessing.Queue, port: int):
    @Request.application
    def oauth_callback(request):
        auth_code = request.args['code']
        state = request.args['state']

        q.put((auth_code, state))
        return Response("", 204)

    run_simple("localhost", port, oauth_callback)


class _AuthenticatedFetcher(fetcher.Fetcher):
    def __init__(self, keypair, access_token):
        self._keypair = keypair
        self._access_token = access_token

    def _get_response(self, url: str):
        headers = {
            'Authorization': ('DPoP ' + self._access_token),
            'DPoP': _make_token_for(self._keypair, url, 'GET')
        }
        response = requests.get(url, headers=headers)
        return response

    def fetch(self, url: str) -> bytes:
        content = self._get_response(url).content
        logging.info("Fetching %s: %s", url, content)
        return content

    def put(self, url: str, content: bytes, mime_type: str) -> None:
        headers = {
            'Authorization': ('DPoP ' + self._access_token),
            'DPoP': _make_token_for(self._keypair, url, 'PUT'),
            'Content-Type': mime_type,
        }
        logging.info("PUT headers: %s", headers)
        response = requests.put(url, headers=headers, data=content)
        logging.info("PUT(%s) -> %d %s %s", url, response.status_code,
                     response.text, response.headers)

    def delete(self, url: str) -> None:
        headers = {
            'Authorization': ('DPoP ' + self._access_token),
            'DPoP': _make_token_for(self._keypair, url, 'DELETE'),
        }
        response = requests.delete(url, headers=headers)
        logging.info("DELETE(%s) -> %d %s %s", url, response.status_code,
                     response.text, response.headers)


def _obtain_keypair_and_access_token(issuer: str, port: int):
    # Provider info discovery.
    # https://pyoidc.readthedocs.io/en/latest/examples/rp.html#provider-info-discovery
    provider_info = requests.get(issuer +
                                 ".well-known/openid-configuration").json()
    logging.info("Provider info: %s", provider_info)
    redirect_url = _get_redirect_url(port)
    client_id = _register_client(provider_info, redirect_url=redirect_url)

    code_verifier, code_challenge = _make_verifier_challenge()

    state = _make_random_string()

    # Generate a key-pair.
    keypair = jwcrypto.jwk.JWK.generate(kty='EC', crv='P-256')

    query = urllib.parse.urlencode({
        "code_challenge": code_challenge,
        "state": state,
        "response_type": "code",
        "redirect_uri": redirect_url,
        "code_challenge_method": "S256",
        "client_id": client_id,
        # offline_access: also asks for refresh token
        "scope": "openid offline_access",
    })
    url = provider_info['authorization_endpoint'] + '?' + query
    logging.info("Please go to: %s", url)

    q = multiprocessing.Queue()
    p = multiprocessing.Process(target=_get_token, args=(q, port))
    p.start()
    print("waiting for auth code & state...")
    auth_code, actual_state = q.get(block=True)
    p.terminate()

    assert state == actual_state

    # Exchange auth code for access token
    resp = requests.post(url=provider_info['token_endpoint'],
                         data={
                             "grant_type": "authorization_code",
                             "client_id": client_id,
                             "redirect_uri": redirect_url,
                             "code": auth_code,
                             "code_verifier": code_verifier,
                         },
                         headers={
                             'DPoP':
                             _make_token_for(keypair,
                                             provider_info['token_endpoint'],
                                             'POST')
                         },
                         allow_redirects=False)

    result = resp.json()
    logging.info("%s", result)

    access_token = result['access_token']
    decoded_access_token = jwcrypto.jwt.JWT()
    decoded_access_token.deserialize(access_token)
    decoded_id_token = jwcrypto.jwt.JWT()
    decoded_id_token.deserialize(result['id_token'])
    logging.info("access token: %s", decoded_access_token)
    logging.info("id token: %s", decoded_id_token)
    return keypair, access_token


def obtain_authenticated_fetcher(issuer: str, port: int):
    keypair, access_token = _obtain_keypair_and_access_token(issuer, port)
    return _AuthenticatedFetcher(keypair=keypair, access_token=access_token)
